import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {HttpClient} from '@angular/common/http';
import {WeatherInfos} from "./weather";

@Injectable({
    providedIn: 'root'
  })
  export class WeatherService {
    constructor(public http: HttpClient) {}

    get(lat: number, lon: number, key: string): Observable<WeatherInfos> {
      return this.http
        .get<WeatherInfos>('https://api.openweathermap.org/data/2.5/weather', {params: {lat, lon, appid: key}});
    }

    getByCity(city: string, key: string): Observable<WeatherInfos> {
      return this.http
        .get<WeatherInfos>('https://api.openweathermap.org/data/2.5/weather', {params: {q: city, appid: key}});
    }
  }
