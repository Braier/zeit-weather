import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Renderer2} from '@angular/core';
import {map, take} from 'rxjs';
import {WeatherInfos} from './weather';
import {WeatherService} from './weather.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {
  public header = '';
  public description = '';
  public imgUrl = ''
  public temperature = 0;
  public city = '';
  public cityModel = ''

  public loading = true;
  public error: string | null = null;

  private localWeather: WeatherInfos | null = null;
  private apiKey = '9c5f3130bb97ba67032fc680a0c3e5be';

  constructor(
    private weatherService: WeatherService,
    private cdr: ChangeDetectorRef,
    private renderer: Renderer2
  ) {
    if (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches) {
      this.renderer.addClass(document.body, 'dark-mode');
    }

    this.submitWithGeo();
  }

  public search() {
    if (this.cityModel !== this.city || this.error) {
      this.getWeatherByCity(this.cityModel);
    }
  }

  public getLocalWeather() {
    if (this.localWeather) {
      this.loading = false;
      this.apply(this.localWeather)
      this.cdr.markForCheck();
    } else {
      this.submitWithGeo();
    }
  }

  private submitWithGeo(store = true) {
    this.loading = true;
    navigator.geolocation.getCurrentPosition(position => {
      this.getWeatherByCoords(position.coords, store)
    }, err => {
      console.log(err);
      this.loading = false;
      this.cdr.markForCheck();
    });
  }

  public getWeatherByCity(city: string) {
    this.loading = true;
    this.error = null;
    this.weatherService.getByCity(city, this.apiKey)
      .pipe(
        take(1),
        map(infos => {
          infos.main.temp = this.kelvinToCelsius(infos.main.temp);
          return infos;
        }))
      .subscribe({
        next: infos => {
          this.loading = false;
          this.apply(infos);
          this.cdr.markForCheck();
        },
        error: err => {
          console.log(err);
          this.error = err.error.message;
          this.loading = false;
          this.cdr.markForCheck();
        }
      });
  }

  private getWeatherByCoords(coords: GeolocationCoordinates, store?: boolean) {
    this.loading = true;
    this.error = null;
    this.weatherService.get(coords.latitude, coords.longitude, this.apiKey)
      .pipe(
        take(1),
        map(infos => {
          infos.main.temp = this.kelvinToCelsius(infos.main.temp);
          return infos;
        }))
      .subscribe({
        next: infos => {
          this.loading = false;
          this.apply(infos, store);
          this.cdr.markForCheck();
        },
        error: err => {
          console.log(err);
          this.error = err.error.message;
          this.loading = false;
          this.cdr.markForCheck();
        }
      });
  }

  private apply(infos: WeatherInfos, store?: boolean) {
    const weather = infos.weather[0];
    const main = infos.main;

    this.header = weather.main;
    this.description = weather.description;
    this.imgUrl = 'http://openweathermap.org/img/wn/' + weather.icon + '@2x.png';
    this.temperature = main.temp;
    this.city = infos.name;

    if (store) {
      this.localWeather = infos;
    }
  }

  private kelvinToCelsius(temp: number): number {
    return Math.round(temp - 273.15);
  }
}
